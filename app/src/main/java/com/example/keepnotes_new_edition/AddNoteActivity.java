package com.example.keepnotes_new_edition;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.keepnotes_new_edition.objects.Note;

public class AddNoteActivity extends AppCompatActivity {

    private DBActions dbActions = new DBActions();
    protected Button insert;
    protected EditText editTitle, editMessage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);


        this.editTitle = findViewById(R.id.editTitle);
        this.editMessage = findViewById(R.id.editMessage);

        findViewById(R.id.btnInsert).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Note note = new Note();
                note.Title = editTitle.getText().toString();
                note.Message = editMessage.getText().toString();

                if(dbActions.insertIntoNotes(note, getFilesDir())){
                    Intent intent = new Intent(AddNoteActivity.this, MainActivity.class);
                    MainActivity.isNotesView = true;
                    startActivity(intent);
                }else {
                    Toast.makeText(getApplicationContext(), "Something went wrong with the insert", Toast.LENGTH_LONG).show();
                }

            }
        });
    }
}