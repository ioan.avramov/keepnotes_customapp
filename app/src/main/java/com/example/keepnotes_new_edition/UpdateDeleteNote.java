package com.example.keepnotes_new_edition;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.keepnotes_new_edition.objects.Note;

public class UpdateDeleteNote extends AppCompatActivity {

    protected EditText titleField, messageField;
    protected Button updateBtn, deleteBtn;
    private DBActions dbActions = new DBActions();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_delete_note);

        Bundle b = getIntent().getExtras();
        int rowID = Integer.parseInt(b.get("rowID").toString());


        Note selectedNote = MainActivity.NOTES_LIST.get(rowID);

        this.titleField = findViewById(R.id.titleField);
        this.messageField = findViewById(R.id.messageField);
        this.updateBtn = findViewById(R.id.btnUpdate);
        this.deleteBtn = findViewById(R.id.btnDelete);

        titleField.setText(selectedNote.Title);
        messageField.setText(selectedNote.Message);

        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedNote.Title = titleField.getText().toString();
                selectedNote.Message = messageField.getText().toString();
                MainActivity.isNotesView = true;
                try {
                    if (dbActions.updateNote(selectedNote, getFilesDir())){
                        Intent intent = new Intent(UpdateDeleteNote.this, MainActivity.class);
                        startActivity(intent);
                    }else{
                        Toast.makeText(getApplicationContext(), "The update returned != 1", Toast.LENGTH_LONG).show();
                    }
                }catch (Exception e ){
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.isNotesView = true;
                try {
                    if (dbActions.deleteNote(selectedNote, getFilesDir())){
                        Intent intent = new Intent(UpdateDeleteNote.this, MainActivity.class);
                        startActivity(intent);
                    }else{
                        Toast.makeText(getApplicationContext(), "The update returned != 1", Toast.LENGTH_LONG).show();
                    }
                }catch (Exception e ){
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });


    }
}