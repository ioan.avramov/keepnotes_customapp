package com.example.keepnotes_new_edition;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.keepnotes_new_edition.objects.Notify;

import java.sql.Time;
import java.util.Calendar;

public class UpdateDeleteNotify extends AppCompatActivity {

    protected EditText notifyField;
    protected TimePicker timePicker;
    protected Button updateBtn, deleteBtn;
    private DBActions dbActions = new DBActions();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_delete_notify);

        Bundle b = getIntent().getExtras();
        int rowId = Integer.parseInt(b.get("rowID").toString());

        Notify selectedNotify = MainActivity.NOTIFY_LIST.get(rowId);

        notifyField = findViewById(R.id.notifyField);
        timePicker = findViewById(R.id.timePickField);
        updateBtn = findViewById(R.id.btnUpdate);
        deleteBtn = findViewById(R.id.btnDelete);

        String[] times = selectedNotify.notifyTime.split(":", 2);
        int hour = Integer.parseInt(times[0]);
        int min = Integer.parseInt(times[1]);
        timePicker.setHour(hour);
        timePicker.setMinute(min);
        timePicker.setEnabled(false);

        notifyField.setText(selectedNotify.Title);

        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedNotify.Title = notifyField.getText().toString();
                int curr_hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
                int cur_min = Calendar.getInstance().get(Calendar.MINUTE);
                int cur_sec = Calendar.getInstance().get(Calendar.SECOND);
                selectedNotify.Delay = AddNotificationActivity.calculateDelay(curr_hour,cur_min,cur_sec, hour, min);
                MainActivity.isNotesView = true;
                try {
                    if(dbActions.updateNotify(selectedNotify, getFilesDir())){
                        Intent intent = new Intent(UpdateDeleteNotify.this, MainActivity.class);
                        startActivity(intent);
                    }
                    else {
                        Toast.makeText(getApplicationContext(), "The update returned != 1", Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                AddNotificationActivity.scheduleNotification(getApplicationContext(), selectedNotify, "You have a notification!",
                        MainActivity.MY_CHANNEL_ID, PendingIntent.FLAG_UPDATE_CURRENT);
            }
        });

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelNotification(selectedNotify);
                MainActivity.isNotesView = true;
                try {
                    if(dbActions.deleteNotify(selectedNotify, getFilesDir())){
                        Intent intent = new Intent(UpdateDeleteNotify.this, MainActivity.class);
                        startActivity(intent);
                    }
                    else {
                        Toast.makeText(getApplicationContext(), "The delete returned != 1", Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
    private void cancelNotification(Notify notify){
        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), MainActivity.MY_CHANNEL_ID )
                .setContentTitle("You have a notification!")
                .setContentText(notify.Title)
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.ic_baseline_notification);

        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        PendingIntent activity = PendingIntent.getActivity(getApplicationContext(), notify.notifyingID, intent, 0);
        builder.setContentIntent(activity);

        Notification notification = builder.build();

        Intent notificationIntent = new Intent(getApplicationContext(), NotificationReceiver.class);
        notificationIntent.putExtra(NotificationReceiver.NOTIFICATION_ID, notify.notifyingID);
        notificationIntent.putExtra(NotificationReceiver.NOTIFICATION, notification);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), notify.Request_Code, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
    }
}