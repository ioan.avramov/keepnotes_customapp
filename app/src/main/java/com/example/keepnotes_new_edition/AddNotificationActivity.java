package com.example.keepnotes_new_edition;


import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.keepnotes_new_edition.objects.Notify;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class AddNotificationActivity extends AppCompatActivity {

    private DBActions dbActions = new DBActions();
    protected TimePicker timePicker;
    protected EditText editNotify;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_notification);

        this.timePicker = findViewById(R.id.timePick);
        this.editNotify = findViewById(R.id.editNotify);

        findViewById(R.id.btnSet).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int hrs = timePicker.getHour();
                int min = timePicker.getMinute();
                int curr_hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
                int cur_min = Calendar.getInstance().get(Calendar.MINUTE);
                int cur_sec = Calendar.getInstance().get(Calendar.SECOND);


                Notify notification = new Notify();
                notification.Delay = calculateDelay(curr_hour, cur_min,cur_sec, hrs, min);
                notification.extraSeconds = Calendar.getInstance().get(Calendar.SECOND)*1000;
                notification.Title = editNotify.getText().toString();
                notification.notifyTime = hrs+":"+min;
                notification.notifyingID = MainActivity.NOTIFICATION_COUNTERS.NotifyingID;
                notification.Request_Code = MainActivity.NOTIFICATION_COUNTERS.Request_Code;
                dbActions.updateNotificationCounters(getFilesDir());

                NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                String CHANNEL_ID = MainActivity.MY_CHANNEL_ID;
                CharSequence name = "my_channel";
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

                    String Description = "This is my channel";
                    int importance = NotificationManager.IMPORTANCE_HIGH;
                    NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
                    mChannel.setDescription(Description);
                    mChannel.enableLights(true);
                    mChannel.setLightColor(Color.RED);
                    mChannel.enableVibration(true);
                    mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                    mChannel.setShowBadge(false);
                    notificationManager.createNotificationChannel(mChannel);
                }
                scheduleNotification(getApplicationContext(), notification,
                        "You have a notification!",  CHANNEL_ID, 0);
                if(dbActions.insertIntoNotifications(notification, getFilesDir())){
                    Intent intent = new Intent(AddNotificationActivity.this, MainActivity.class);
                    intent.putExtra("fragment", "SecondFragment");
                    MainActivity.isNotesView = true;
                    startActivity(intent);
                }else {
                    Toast.makeText(getApplicationContext(), "Something went wrong with the set", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
    public static void scheduleNotification(Context context, Notify notify,
                                     String title, String channel_Id, int intentFlag) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, channel_Id)
                .setContentTitle(title)
                .setContentText(notify.Title)
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.ic_baseline_notification);

        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent activity = PendingIntent.getActivity(context, notify.notifyingID, intent, 0);
        builder.setContentIntent(activity);

        Notification notification = builder.build();

        Intent notificationIntent = new Intent(context, NotificationReceiver.class);
        notificationIntent.putExtra(NotificationReceiver.NOTIFICATION_ID, notify.notifyingID);
        notificationIntent.putExtra(NotificationReceiver.NOTIFICATION, notification);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, notify.Request_Code, notificationIntent, intentFlag);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        long afterTime = System.currentTimeMillis() + notify.Delay;
        alarmManager.set(AlarmManager.RTC_WAKEUP, afterTime , pendingIntent);
    }

    public static long calculateDelay(int currentHour, int currentMinute, int currentSeconds,  int selectedHour, int selectedMinute){

        long difference = 0;
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        Date timeNow = null;
        Date timeSelected = null;
        String time1= selectedHour+":"+selectedMinute+":00";
        String time2 = currentHour+":"+currentMinute+":"+currentSeconds;
        try {
            timeSelected = format.parse(time1);
            timeNow = format.parse(time2);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        c1.setTime(timeNow);
        c2.setTime(timeSelected);

        difference = timeNow.getTime() - timeSelected.getTime();
        if(difference > 0) {
            c2.set(Calendar.DAY_OF_YEAR, c2.get(Calendar.DAY_OF_YEAR) + 1);
        }
        long elapsed = c2.getTimeInMillis() - c1.getTimeInMillis();

        return elapsed;

    }
}
