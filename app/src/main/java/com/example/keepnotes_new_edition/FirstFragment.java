package com.example.keepnotes_new_edition;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.example.keepnotes_new_edition.objects.Note;

import java.util.ArrayList;

public class FirstFragment extends Fragment{

    protected DBActions dbActions = new DBActions();
    protected ListView lv;
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        return inflater.inflate(R.layout.fragment_first, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.notifications_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.isNotesView = false;
                NavHostFragment.findNavController(FirstFragment.this)
                        .navigate(R.id.action_FirstFragment_to_SecondFragment);
            }
        });
        lv = getView().findViewById(R.id.notesList);
        Cursor response = dbActions.selectAll("Notes", getContext().getFilesDir());
        MainActivity.NOTES_LIST = extractData(response);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String> (
                getContext(),
                R.layout.activity_text_view,
                R.id.textView,
                getTitles(MainActivity.NOTES_LIST));
        lv.setAdapter(arrayAdapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getContext(), UpdateDeleteNote.class);
                intent.putExtra("rowID", id);
                startActivity(intent);
            }
        });
    }

    private  ArrayList<Note> extractData(Cursor dataSource){
        ArrayList<Note> list = new ArrayList<>();
        try {
            if (dataSource.moveToFirst()) {
                do {
                    Note note = new Note();
                    note.ID = (dataSource.getInt(0));
                    note.Title = (dataSource.getString(1));
                    note.Message = (dataSource.getString(2));
                    list.add(note);
                } while (dataSource.moveToNext());
            }

        } finally {
            try { dataSource.close(); } catch (Exception ignore) {}
            return list;
        }
    }
    private ArrayList<String> getTitles(ArrayList<Note> noteList){
        ArrayList<String> titles = new ArrayList<>();
        for (Note n:
                noteList) {
            titles.add(n.Title);
        }
        return titles;
    }

}