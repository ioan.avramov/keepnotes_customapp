package com.example.keepnotes_new_edition;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.example.keepnotes_new_edition.objects.NotificationCounters;
import com.example.keepnotes_new_edition.objects.Notify;

import java.util.ArrayList;

public class SecondFragment extends Fragment {

    protected DBActions dbActions = new DBActions();
    protected ListView lv;
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.notes_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.isNotesView = true;
                NavHostFragment.findNavController(SecondFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });

        lv = (ListView) getView().findViewById(R.id.notificationList);

        Cursor response = dbActions.selectAll("Notifications", getContext().getFilesDir());
        MainActivity.NOTIFY_LIST = extractData(response);
        MainActivity.NOTIFICATION_COUNTERS = getNotificationCounters(
                dbActions.selectAll("NotificationCounters", getContext().getFilesDir()));

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String> (
                getContext(),
                R.layout.activity_text_view,
                R.id.textView,
                getTitles(MainActivity.NOTIFY_LIST));
        lv.setAdapter(arrayAdapter);


        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getContext(), UpdateDeleteNotify.class);
                intent.putExtra("rowID", id);
                startActivity(intent);
            }
        });

    }
    private NotificationCounters getNotificationCounters(Cursor cursor){
        NotificationCounters nc = new NotificationCounters();
        int rows = cursor.getCount(); //for testing
        try {
            if (cursor.moveToFirst()) {
                nc.ID = (cursor.getInt(0));
                nc.NotifyingID = cursor.getInt(1);
                nc.Request_Code =  cursor.getInt(2);
            }

        } finally {
            try { cursor.close(); } catch (Exception ignore) {}
            return nc;
        }
    }

    private ArrayList<Notify> extractData(Cursor dataSource){
        ArrayList<Notify> list = new ArrayList<>();
        try {
            if (dataSource.moveToFirst()) {
                do {
                    Notify noti = new Notify();
                    noti.ID = (dataSource.getInt(0));
                    noti.Title = (dataSource.getString(1));
                    noti.notifyTime =  dataSource.getString(2);
                    noti.notifyingID = dataSource.getInt(3);
                    noti.Request_Code = dataSource.getInt(4);
                    noti.Delay = dataSource.getInt(5);
                    noti.extraSeconds = dataSource.getInt(6);
                    list.add(noti);
                } while (dataSource.moveToNext());
            }

        } finally {
            try { dataSource.close(); } catch (Exception ignore) {}
            return list;
        }
    }
    private ArrayList<String> getTitles(ArrayList<Notify> noteList){
        ArrayList<String> titles = new ArrayList<>();
        for (Notify n:
                noteList) {
            titles.add(n.Title + " NID:"+n.notifyingID + " RC:"+n.Request_Code + " De:"+n.Delay);
        }
        return titles;
    }
}