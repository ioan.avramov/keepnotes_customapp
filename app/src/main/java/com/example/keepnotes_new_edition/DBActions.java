package com.example.keepnotes_new_edition;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.keepnotes_new_edition.objects.Note;
import com.example.keepnotes_new_edition.objects.NotificationCounters;
import com.example.keepnotes_new_edition.objects.Notify;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;

public class DBActions extends AppCompatActivity {

    private String DB_VERSION = "10";

    protected void initDb(File file) throws SQLException{

        SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(
                file.getPath() + "/keepnotes"+DB_VERSION+".db" ,
                null);

        String sql = "Create table if not exists Notes(" +
                     "ID integer Primary Key AUTOINCREMENT," +
                     "Title text not null," +
                     "Message text not null)";
        db.execSQL(sql);
        sql = "Create table if not exists Notifications(" +
                "ID integer Primary Key AUTOINCREMENT," +
                "Title text not null," +
                "NotifyTime text not null," +
                "NotifyingID integer not null," +
                "Request_Code integer not null," +
                "Delay integer not null, " +
                "Extra_Seconds integer not null)";
        db.execSQL(sql);
        sql = "Create table if not exists NotificationCounters(" +
                "ID integer Primary Key AUTOINCREMENT," +
                "NotifyingID integer not null," +
                "Request_Code integer not null)";
        db.execSQL(sql);
        sql = "INSERT OR IGNORE INTO NotificationCounters " +
              "(ID, NotifyingID, Request_Code) " +
              "values (0,0,0)";
        db.execSQL(sql);
        db.close();
    }
    public void updateNotificationCounters(File file){
        SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(
                file.getPath()+  "/keepnotes"+DB_VERSION+".db",
                null);
       String sql = "UPDATE NotificationCounters " +
              "SET NotifyingID = NotifyingID + 1, " +
              "Request_Code = Request_Code + 1 " +
              "WHERE ID=0";

        db.execSQL(sql);
    }
    public Cursor selectAll(String tableName, File file){
        SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(
                file.getPath() +  "/keepnotes"+DB_VERSION+".db",
                null);

        return db.query(tableName,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null);
    }
    public boolean insertIntoNotes(Note note, File file){
        SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(
                file.getPath() +  "/keepnotes"+DB_VERSION+".db",
                null);

        ContentValues contentValues = new ContentValues();
        contentValues.put("Title", note.Title);
        contentValues.put("Message", note.Message);
        if (db.insert("Notes", null, contentValues) > 0){
            return true;
        }
        return false;
    }
    public boolean insertIntoNotifications(Notify notification, File file){
        SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(
                file.getPath() +  "/keepnotes"+DB_VERSION+".db",
                null);

        ContentValues contentValues = new ContentValues();
        contentValues.put("Title", notification.Title);
        contentValues.put("NotifyTime", notification.notifyTime);
        contentValues.put("NotifyingID", notification.notifyingID);
        contentValues.put("Request_Code", notification.Request_Code);
        contentValues.put("Delay", notification.Delay);
        contentValues.put("Extra_Seconds", notification.extraSeconds);
        if (db.insert("Notifications", null, contentValues) > 0){
            return true;
        }
        return false;
    }
    public boolean updateNote(Note note, File file){
        SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(
                file.getPath()+  "/keepnotes"+DB_VERSION+".db",
                null);
        ContentValues contentValues = new ContentValues();
        contentValues.put("Title", note.Title);
        contentValues.put("Message", note.Message);
        if (db.update("Notes",
                         contentValues,
                        "ID="+note.ID,
                        null) == 1){
            return true;
        }
        return false;

    }
    public boolean updateNotify(Notify notify, File file){
        SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(
                file.getPath()+  "/keepnotes"+DB_VERSION+".db",
                null);
        ContentValues contentValues = new ContentValues();
        contentValues.put("Title", notify.Title);
        contentValues.put("Delay", notify.Delay);
        if (db.update("Notifications",
                contentValues,
                "ID="+notify.ID,
                null) == 1){
            return true;
        }
        return false;
    }
    public boolean deleteNotify(Notify notify, File file){
        SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(
                file.getPath()+  "/keepnotes"+DB_VERSION+".db",
                null);

        if (db.delete("Notifications",
                "ID="+notify.ID,
                null) == 1){
            return true;
        }
        return false;
    }
    public boolean deleteNote(Note note, File file){
        SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(
                file.getPath()+  "/keepnotes"+DB_VERSION+".db",
                null);

        if (db.delete("Notes",
                "ID="+note.ID,
                null) == 1){
            return true;
        }
        return false;
    }
}
