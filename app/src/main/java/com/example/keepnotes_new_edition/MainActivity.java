package com.example.keepnotes_new_edition;

import android.app.AlarmManager;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;

import com.example.keepnotes_new_edition.objects.Note;
import com.example.keepnotes_new_edition.objects.NotificationCounters;
import com.example.keepnotes_new_edition.objects.Notify;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.fragment.NavHostFragment;

import android.view.View;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    //Global variables
    protected static boolean isNotesView = true;
    protected static NotificationCounters NOTIFICATION_COUNTERS = new NotificationCounters();
    protected static ArrayList<Note> NOTES_LIST = new ArrayList<>();
    protected static ArrayList<Notify> NOTIFY_LIST = new ArrayList<>();
    protected static String MY_CHANNEL_ID = "my_channel_01";


    protected FloatingActionButton floatingAddButton;
    protected DBActions dbActions = new DBActions();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        try {
            dbActions.initDb(getFilesDir());
        }catch (Exception e){
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        floatingAddButton = findViewById(R.id.add);
        floatingAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               if (isNotesView){
                   Intent intent = new Intent(MainActivity.this, AddNoteActivity.class);
                   startActivity(intent);
               }
               else {
                   Intent intent = new Intent(MainActivity.this, AddNotificationActivity.class);
                   startActivity(intent);
               }
            }
        });

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
}